require 'slack-ruby-client'

Slack.configure do |config|
  config.token = ENV['SLACK_API_TOKEN']
  fail 'Missing ENV[SLACK_API_TOKEN]!' unless config.token
end

client = Slack::Web::Client.new

attachments = <<EOF
[{
	"text": "button",
	"actions": [{
		"name": "aaa",
		"text": "AAA",
		"type": "button",
		"value": "A"
	},
	{
		"name": "bbb",
		"text": "BBB",
		"type": "button",
		"value": "B"
	}
]
}]
EOF

client.chat_postMessage(channel: '#test', text: 'button', attachments: attachments)
