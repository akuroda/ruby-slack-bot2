# coding: utf-8
require 'slack-ruby-client'
require 'json'
require 'stringio'
require './slack_button'


Slack.configure do |config|
  config.token = ENV['SLACK_API_TOKEN']
  fail 'Missing ENV[SLACK_API_TOKEN]!' unless config.token
  config.logger = Logger.new(STDOUT)
  config.logger.level = Logger::INFO
end

client = Slack::Web::Client.new

country = Hash.new
File.open("data/country.json") do |file| 
  country = JSON.parse(file.read)
end

c0 = country.keys.sample
buttons = Array.new
buttons.push ActionButton.new(c0, c0, c0)

c = country.keys.sample
buttons.push ActionButton.new(c, c, c)

c = country.keys.sample
buttons.push ActionButton.new(c, c, c)
buttons = buttons.shuffle

attachments = Attachments.new("こっき", country[c0], buttons)
puts attachments

client.chat_postMessage(channel: '#test', attachments: attachments.to_s)
