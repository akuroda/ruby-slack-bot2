# coding: utf-8
require 'csv'

keys = %w!country capital region flag_url!
countries = []
CSV.read(ARGV[0]).map do |a|
  countries.push Hash[keys.zip(a)]
end

euro = countries.select do |c|
  c['region'] == 'ヨーロッパ'
end

puts euro
