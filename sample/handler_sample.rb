@handlers = []

def add_handler(regex, &action)
  @handlers.push [regex, action]
end

add_handler /hello (.*)/ do
  puts 'hello world'
  puts @matchstr[1]
end

add_handler /bye/ do
  puts 'bye!'
end

def main str
  @handlers.each do |handler|
    if m = handler[0].match(str)
      @matchstr = m
      handler[1].call
      break
    end
  end
end

main 'hello world'
main 'hello ruby'
main 'bye'
