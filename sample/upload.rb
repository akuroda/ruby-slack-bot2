# coding: utf-8
require 'slack-ruby-client'
require 'net/http/post/multipart'
require 'csv'
require 'stringio'

Slack.configure do |config|
  config.token = ENV['SLACK_API_TOKEN']
  fail 'Missing ENV[SLACK_API_TOKEN]!' unless config.token
  config.logger = Logger.new(STDOUT)
  config.logger.level = Logger::INFO
end

client = Slack::Web::Client.new

csv = CSV.read("data/country.csv")
country = Hash.new
csv.each do |data|
  country[data[0]] = data[1]
end

c = country.keys.sample


http_conn = Faraday.new do |builder|
  builder.adapter Faraday.default_adapter
end 

response = http_conn.get country[c]
buf = StringIO.new(response.body)


client.files_upload(
  channels: '#test',
  file: UploadIO.new(buf, 'image/png', 'country.jpg'),
  title: "こっき",
  filename: 'country.jpg',
#  initial_comment: c
)
