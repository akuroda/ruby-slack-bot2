# coding: utf-8
require 'slack-ruby-client'
require 'json'
require 'stringio'


Slack.configure do |config|
  config.token = ENV['SLACK_API_TOKEN']
  fail 'Missing ENV[SLACK_API_TOKEN]!' unless config.token
  config.logger = Logger.new(STDOUT)
  config.logger.level = Logger::INFO
end

client = Slack::Web::Client.new

a = rand(50)
b = rand(50)

client.chat_postMessage(channel: '#test', text: "#{a} + #{b}")
