# coding: utf-8

require 'slack-ruby-client'
require 'mechanize'
require 'date'
require 'time'
require 'optparse'

LIBRARY_URL = 'https://www.library.city.chuo.tokyo.jp/'

def login(page, user)
  loginPage = page.link_with(:href => /linkLogin/).click
  puts loginPage.uri

  form = loginPage.form_with(:action => /inputForm/)
  puts form.action
  form.field_with(:name => /textUserId/).value = user[:id]
  form.field_with(:name => /textPassword/).value = user[:pass]
  page = form.click_button
end

def set_cookie(agent)
  # 一覧表示にするためにcookieをセット
  uri = URI.parse(LIBRARY_URL)
  cookie = Mechanize::Cookie.new('mode', uri.host, {:value=>"1",:domain=>uri.host, :path=>"/"}) 
  agent.cookie_jar << cookie
end


def getRentalList(user)
  agent = Mechanize.new
  page = agent.get(LIBRARY_URL)

  page = login(page, user)
  puts "login OK"
  set_cookie(agent)

  page = page.link_with(:href => /rentallist/).click
  puts page.uri
  books = []
  today = Date.today

  page.search('table/tr').each do |tr|
    # xpath result include tag
    if tr.xpath('td')[1] && tr.xpath('td')[4]
      title = tr.xpath('td[3]/a/span')[0].to_s.gsub(/<\/?[^>]*>/, '')
      jdate = tr.xpath('td')[5].to_s.gsub(/<\/?[^>]*>/, '')
      due_date = Time.strptime(jdate, "%Y年%m月%d日").to_date
      suffix = ""
      if today > due_date
        suffix = " :exclamation:"
      elsif due_date - today <= 3
        suffix = " :warning:"
      end
      books.push title + " " + jdate + suffix
      puts title + " " + jdate + suffix
    end
  end
  # logout
  page.link_with(:href => /linkLogout/).click
  puts "logout"

  books
end


def getReserveList(user)
  agent = Mechanize.new
  page = agent.get(LIBRARY_URL)

  page = login(page, user)
  set_cookie(agent)

  page = page.link_with(:href => /reservelist/).click
  books = []

  page.search('table/tr').each do |tr|
    if tr.xpath('td')[1] && tr.xpath('td')[4]
      title = tr.xpath('td')[1].to_s.gsub(/<\/?[^>]*>/, '').strip
      status = tr.xpath('td')[4].to_s.gsub(/<\/?[^>]*>/, '').strip

      books.push title + " " + status
      puts title + " " + status
    end
  end
  # logout
  page.link_with(:href => /linkLogout/).click
  puts "logout"

  books
end

# s: output to slack
params = ARGV.getopts('s')
client = nil

users = [
  { name: "あきら", id: ENV['LIBRARY_USER_0'], pass: ENV['LIBRARY_PASS_0']},
  { name: "ともこ", id: ENV['LIBRARY_USER_1'], pass: ENV['LIBRARY_PASS_1']}
#  { name: "まゆ", id: ENV['LIBRARY_USER_2'], pass: ENV['LIBRARY_PASS_2']}
]

if params['s']
  Slack.configure do |config|
    config.token = ENV['SLACK_API_TOKEN']
    fail 'Missing ENV[SLACK_API_TOKEN]!' unless config.token
  end

  client = Slack::Web::Client.new
end

users.each do |u| 
  puts u
  if !ARGV[0] || ARGV[0] =~ /rental/
    books = getRentalList u
  elsif ARGV[0] =~ /(reserve|reservation)/
    books = getReserveList u
  end
  if params['s'] then client.chat_postMessage(channel: '#test', text: books.join("\n")) end
end
