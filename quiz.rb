# coding: utf-8

require 'csv'
require 'json'

class Quiz
  attr_accessor :question, :answer, :description
  @ok_messages = %w#せいかい！ あたり！ おみごと！#
  @ok_suffix = %w#:tada: :confetti_ball: :v: :smile: :ok_woman:#
  @ng_messages = %w#ざんねん はずれ #
  @ng_suffix = %w#:sob: :crying_cat_face: :fearful: :no_good:#

  def initialize(question, answer, description=nil)
    @question = question
    @answer = answer
    @description = description
  end

  def Quiz.ok_msg
    return @ok_messages.sample + @ok_suffix.sample
  end

  def Quiz.ng_msg
    return @ng_messages.sample + @ng_suffix.sample
  end

end

class MathQuiz
  def add_quiz
    r = rand(2)
    if r == 0
      add_quiz2
    else
      add_quiz3_4
    end
  end

  def add_quiz2
    a = rand(100)
    b = rand(100)
    Quiz.new("#{a} + #{b}", a + b)
  end

  def add_quiz3_4
    num = rand(3 .. 4)
    a = Array.new
    (1 .. num).each do |n|
      a.push rand(1 .. 19)
    end
    Quiz.new(a.join(" + "), a.inject(:+))
  end

  def sub_quiz
    if rand(2) == 0
      sub_quiz2
    else
      sub_quiz3
    end
  end

  def sub_quiz2
    a = rand(1 .. 50)
    b = rand(1 .. a)
    Quiz.new("#{a} - #{b}", a - b)
  end

  def sub_quiz3
    a = rand(1 .. 50)
    b = rand(1 .. a)
    c = rand(1 .. a - b)
    Quiz.new("#{a} - #{b} - #{c}", a - b - c)
  end

  def mul_quiz
    a = rand(1 .. 9)
    b = rand(1 .. 9)
    Quiz.new("#{a} * #{b}", a * b)
  end

  def div_quiz
    a = rand(1 .. 9)
    b = rand(2 .. 9)
    Quiz.new("#{a * b} / #{b}", a)
  end

  def fraction_quiz
    a = rand(1 .. 9)
    b = rand(2 .. 9)
    c = rand(1 .. 9)
    d = rand(2 .. 9)
    Quiz.new("#{a}/#{b}+#{c}/#{d}", (Rational(a, b) + Rational(c, d)).to_s)
  end
end


module ChoiceQuiz
  def choice3 qarray, qname, aname, dname=nil
    copy_q = qarray.dup
    quizzez = Array.new
    (0..2).each do
      copy_q.shuffle!
      e = copy_q.pop
      if dname
        quizzez.push Quiz.new(e[qname], e[aname], e[dname])
      else
        quizzez.push Quiz.new(e[qname], e[aname])
      end
    end
    quizzez
  end
end

class FlagQuiz
  attr_accessor :country, :quizzes
  include ChoiceQuiz

  def initialize
    keys = %w!country capital region flag_url!
    @country = []
    CSV.read('data/country.csv', encoding: 'utf-8').map do |a|
      @country.push Hash[keys.zip(a)]
    end

  end

  def flag_quiz region
    @quizzes = Array.new 
    if region.nil?
      countries = @country
    else
      countries = @country.select do |c|
        c['region'] == region
      end
    end
    choice3 countries, 'flag_url', 'country'
  end

  def capital_quiz region
    @quizzes = Array.new
    if region.nil?
      countries = @country
    else
      countries = @country.select do |c|
        c['region'] == region
      end
    end
    choice3 countries, 'country', 'capital'
  end
end

class KotowazaQuiz
  attr_accessor :kotowaza, :quizzez
  include ChoiceQuiz

  def initialize
    @kotowaza = Array.new
    File.open("data/kotowaza.json") do |file|
      @kotowaza = JSON.parse(file.read)
    end
  end

  def kotowaza_quiz
    choice3 @kotowaza, 'top', 'bottom'
  end
end

class EnglishQuiz
  attr_accessor :english, :quizzez
  include ChoiceQuiz

  def initialize
    @english = Array.new
    File.open("data/english.json") do |file|
      @english = JSON.parse(file.read)
    end
  end

  def english_quiz
    if rand(2) == 0
      choice3 @english, 'en', 'ja'
    else
      choice3 @english, 'ja', 'en'
    end
  end
end

class WesternArtQuiz
  attr_accessor :westernart, :quizzez
  include ChoiceQuiz

  def initialize
    @westernart = Array.new
    File.open("data/western-art.json") do |file|
      @westernart = JSON.parse(file.read)
    end
  end

  def westernart_quiz
    if rand(2) == 0
      choice3 @westernart, 'image', 'creator', 'name'
    else
      choice3 @westernart, 'image', 'name', 'creator'
    end
  end
end
