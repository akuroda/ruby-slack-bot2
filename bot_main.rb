# coding: utf-8

require './slack_button'
require './quiz'
require './library'

def choice_quiz(choices, desc, image_quiz=false)
  buttons = Array.new
  
  c0 = choices.sample
  choices.each do |q|
    buttons.push ActionButton.new(q.answer, q.answer, q.answer)
  end

  if !image_quiz
    attachments = Attachments.new(c0.question, "", buttons)
  else
    text = c0.description || desc
    attachments = Attachments.new(text, c0.question, buttons)
  end

  puts attachments

  client = Slack::Web::Client.new
  client.chat_postMessage(channel: '#test', text:"[#{desc}]", attachments: attachments.to_s)
end


@handlers = []

def add_handler(regex, &action)
  @handlers.push [regex, action]
end

add_handler /bot hi/ do
  @client.message channel: @data['channel'], text: "Hi <@#{@data['user']}>!"
end

add_handler /bot bye/ do
  @client.stop!
end

add_handler /^(いま|今)?(なんじ|何時)/ do
  t = Time.now
  @client.message channel: @data['channel'], text: t.strftime("%I:%M:%S %p") + "です"
end

add_handler /^(きょう|今日)?(なんにち|何日)/ do
  d = Time.now
  wdays = ["日", "月", "火", "水", "木", "金", "土"]
  @client.message channel: @data['channel'], text: d.strftime("%-m/%-d") + "(#{wdays[d.wday]})です"
end

add_handler /^(こっき|国旗)[ 　]?(ヨーロッパ|アジア|アメリカ|アフリカ)?/ do
  choice_quiz @fq.flag_quiz(@matchstr[2]), "こっき", true
end

add_handler /^(しゅと|首都)[ 　]?(ヨーロッパ|アジア|アメリカ|アフリカ)?/ do
  choice_quiz @fq.capital_quiz(@matchstr[2]), "しゅと"
end

add_handler /^(たしざん|足し算)/ do
  q = @mq.add_quiz
  @webClient.chat_postMessage(channel: '#test', text: q.question)
  @client.prev = q.answer
end

add_handler /^(ひきざん|引き算)/ do
  q = @mq.sub_quiz
  @webClient.chat_postMessage(channel: '#test', text: q.question)
  @client.prev = q.answer
end

add_handler /^(かけざん|掛け算)/ do
  q = @mq.mul_quiz
  @webClient.chat_postMessage(channel: '#test', text: q.question)
  @client.prev = q.answer
end

add_handler /^(わりざん|割り算)/ do
  q = @mq.div_quiz
  @webClient.chat_postMessage(channel: '#test', text: q.question)
  @client.prev = q.answer
end

add_handler /^(ぶんすう|分数)/ do
  q = @mq.fraction_quiz
  @webClient.chat_postMessage(channel: '#test', text: q.question)
  @client.prev = q.answer
end

add_handler /^=(\d+)$/ do
  if @client.prev != -1
    if @client.prev == @matchstr[1].to_i
      @client.message channel: @data['channel'], text: Quiz.ok_msg
      @client.prev = -1
    else
      @client.message channel: @data['channel'], text: Quiz.ng_msg
    end
  end
end

# answer for fraction quiz
add_handler /^=(\d+\/\d+)/ do
  if @client.prev != -1
    if @client.prev == @matchstr[1]
      @client.message channel: @data['channel'], text: Quiz.ok_msg
      @client.prev = -1
    else
      @client.message channel: @data['channel'], text: Quiz.ng_msg
    end
  end
end

add_handler /^(としょかん|図書館)/ do
  l = LibraryCmd.new
  l.users.each do |u|
    books = l.rental_list u
    if books
      text = u[:name] + " " + u[:id] + "\n" + books.join("\n")
      @webClient.chat_postMessage channel: '#test', text: text
    end
  end
end

add_handler /^(よやく|予約)/ do
  l = LibraryCmd.new
  l.users.each do |u|
    books = l.reserve_list u
    if books
      text = u[:name] + " " + u[:id] + "\n" + books.join("\n")
      @webClient.chat_postMessage channel: '#test', text: text
    end
  end
end

add_handler /^おみくじ/ do
  omikuji = ["大吉", "中吉", "小吉", "吉", "末吉", "凶"]
  @webClient.chat_postMessage channel: '#test', text: omikuji.sample
end

add_handler /^ことわざ/ do
  choice_quiz @kq.kotowaza_quiz, "ことわざ"
end

add_handler /^えいご/ do
  choice_quiz @eq.english_quiz, "えいご"
end

add_handler /^(せいよう|西洋)/ do
  q = @waq.westernart_quiz
  choice_quiz q, "西洋美術", true
end


def dispatch(data)
  @handlers.each do |handler|
    if m = handler[0].match(data['text'])
      @matchstr = m
      @data = data
      handler[1].call
      break
    elsif  data.has_key?('attachments') &&  m = handler[0].match(data['attachments'][0]['pretext'])
      # react to another bot(IFTTT)
      @matchstr = m
      @data = data
      handler[1].call
      break
    end
  end
end
