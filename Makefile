# ignore test directory
.PHONY: test
test:
	bundle exec rake test

deploy:
	ibmcloud cf push -b https://github.com/cloudfoundry/ruby-buildpack.git

endpoint:
 ibmcloud target --cf-api https://api.ng.bluemix.net -o moonisland -s dev
