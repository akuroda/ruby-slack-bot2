# coding: utf-8

require 'mechanize'
require 'date'
require 'time'

class LibraryCmd
  LIBRARY_URL = 'https://www.library.city.chuo.tokyo.jp/'

  attr_accessor :users

  def initialize
    @users = [
      { name: "あきら", id: ENV['LIBRARY_USER_0'], pass: ENV['LIBRARY_PASS_0']},
      { name: "ともこ", id: ENV['LIBRARY_USER_1'], pass: ENV['LIBRARY_PASS_1']},
      { name: "まゆ", id: ENV['LIBRARY_USER_2'], pass: ENV['LIBRARY_PASS_2']}
    ]
  end

  def login(page, user)
    loginPage = page.link_with(:href => /linkLogin/).click

    form = loginPage.form_with(:action => /inputForm/)
    form.field_with(:name => /textUserId/).value = user[:id]
    form.field_with(:name => /textPassword/).value = user[:pass]
    page = form.click_button
  end

  def set_cookie(agent)
    # 一覧表示にするためにcookieをセット
    uri = URI.parse(LIBRARY_URL)
    cookie = Mechanize::Cookie.new('mode', uri.host, {:value=>"1",:domain=>uri.host, :path=>"/"}) 
    agent.cookie_jar << cookie
  end

  def rental_list(user)
    agent = Mechanize.new
    page = agent.get(LIBRARY_URL)

    page = login(page, user)
    set_cookie(agent)

    page = page.link_with(:href => /rentallist/).click
    books = []
    today = Date.today

    page.search('table/tr').each do |tr|
      if tr.xpath('td')[1] && tr.xpath('td')[4]
        # xpath result include tag
        title = tr.xpath('td[3]/a/span')[0].to_s.gsub(/<\/?[^>]*>/, '')
        jdate = tr.xpath('td')[5].to_s.gsub(/<\/?[^>]*>/, '')
        due_date = Time.strptime(jdate, "%Y年%m月%d日").to_date
        suffix = ""
        if today > due_date
          suffix = " :exclamation:"
        elsif due_date - today <= 3
          suffix = " :warning:"
        end
        books.push title + " " + jdate + suffix
#        puts tr.xpath('td')[1].to_s.gsub(/<\/?[^>]*>/, '')
      end
    end
    # logout
    page.link_with(:href => /linkLogout/).click

    books
  end

  def reserve_list(user)
    agent = Mechanize.new
    page = agent.get(LIBRARY_URL)

    page = login(page, user)
    set_cookie(agent)

    page = page.link_with(:href => /reservelist/).click
    books = []

    page.search('table/tr').each do |tr|
      if tr.xpath('td')[1] && tr.xpath('td')[4]
        title = tr.xpath('td')[1].to_s.gsub(/<\/?[^>]*>/, '').strip
        status = tr.xpath('td')[4].to_s.gsub(/<\/?[^>]*>/, '').strip

        books.push title + " " + status
        puts title + " " + status
      end
    end
    books
  end
end
