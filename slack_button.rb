class Attachments
  attr_accessor :text, :image_url, :buttons
  def initialize(text, image_url, buttons)
    @text = text
    @image_url = image_url
    @buttons = buttons
  end

  def to_s
    actions = @buttons.join(",")
    attachments = <<EOF
[{
	"text": "#{@text}",
  "image_url": "#{@image_url}",
	"actions": [#{actions}]
}]
EOF
  end
end

class ActionButton
  attr_accessor :name, :text, :value

  def initialize(name, text, value)
    @name = name
    @text = text
    @value = value
  end

  def to_s
    %Q!{"name": "#{@name}", "text": "#{@text}", "type": "button", "value": "#{@value}"}!
  end
end
