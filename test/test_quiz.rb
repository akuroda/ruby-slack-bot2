# coding: utf-8

require 'test/unit'
require './quiz'

class TestQuiz < Test::Unit::TestCase
  def setup
    srand(42)
  end

  def test_initialize
    q = Quiz.new('foo', 'bar')
    assert_equal('foo', q.question)
    assert_equal('bar', q.answer)
  end

  def test_ok_msg
    assert_equal('おみごと！:smile:', Quiz.ok_msg)
    assert_equal('せいかい！:v:', Quiz.ok_msg)
  end

  def test_ng_msg
    assert_equal('ざんねん:no_good:', Quiz.ng_msg)
    assert_equal('ざんねん:fearful:', Quiz.ng_msg)
  end

end

class TestMathQuiz < Test::Unit::TestCase
  def setup
    srand(42)
  end

  def test_add_quiz
    q = MathQuiz.new.add_quiz
    assert_equal(q.class, Quiz)
  end

  def test_add_quiz2
    q = MathQuiz.new.add_quiz2
    assert_equal('51 + 92', q.question)
    assert_equal(143, q.answer)
  end

  def test_add_quiz3_4
    q = MathQuiz.new.add_quiz3_4
    assert_equal('15 + 11 + 8', q.question)
    assert_equal(34, q.answer)

    srand(1)
    q = MathQuiz.new.add_quiz3_4
    assert_equal('12 + 13 + 9 + 10', q.question)
    assert_equal(44, q.answer)
  end

  def test_sub_quiz2
    q = MathQuiz.new.sub_quiz2
    assert_equal('39 - 29', q.question)
    assert_equal(10, q.answer)
  end

  def test_sub_quiz3
    q = MathQuiz.new.sub_quiz3
    assert_equal('39 - 29 - 8', q.question)
    assert_equal(2, q.answer)
  end

  def test_mul_quiz
    q = MathQuiz.new.mul_quiz
    assert_equal('7 * 4', q.question)
    assert_equal(28, q.answer)
  end

  def test_div_quiz
    q = MathQuiz.new.div_quiz
    assert_equal('35 / 5', q.question)
    assert_equal(7, q.answer)
  end

  def test_fraction_quiz
    q = MathQuiz.new.fraction_quiz
    assert_equal('7/5+8/6', q.question)
    assert_equal('41/15', q.answer)
  end

  class TestFlagQuiz < Test::Unit::TestCase
    def setup
      @fq = FlagQuiz.new
    end

    def test_initialize
      assert_true(@fq.country.is_a?(Array))
      keys = %w!country capital region flag_url!

      @fq.country.each do |c|
        keys.each do |k|
          assert_true(c.has_key?(k))
        end
      end
    end

    def test_flag_quiz
      quizzes = @fq.flag_quiz nil
      assert_true(quizzes.is_a?(Array))
      assert_equal(3, quizzes.length)
      quizzes.each do |q|
        assert_equal(0, q.question =~ URI::regexp(%w(http https)))
      end

      quizzes = @fq.flag_quiz "アジア"
      assert_true(quizzes.is_a?(Array))
      assert_equal(3, quizzes.length)
      quizzes.each do |q|
        assert_equal(0, q.question =~ URI::regexp(%w(http https)))
      end
    end

    def test_capital_quiz
      quizzes = @fq.capital_quiz nil
      assert_true(quizzes.is_a?(Array))
      assert_equal(3, quizzes.length)

      quizzes = @fq.capital_quiz "アフリカ"
      assert_true(quizzes.is_a?(Array))
      assert_equal(3, quizzes.length)
    end
  end

  class TestKotowazaQuiz < Test::Unit::TestCase
    def setup
      @kq = KotowazaQuiz.new
    end

    def test_initialize
      assert_true(@kq.kotowaza.is_a?(Array))
      keys = %w!start top bottom!

      @kq.kotowaza.each do |kw|
        keys.each do |k|
          assert_true(kw.has_key?(k))
        end
      end
    end

    def test_kotowaza_quiz
      quizzes = @kq.kotowaza_quiz
      assert_true(quizzes.is_a?(Array))
      assert_equal(3, quizzes.length)
      quizzes.each do |q|
        assert_not_nil(q.question)
        assert_not_nil(q.answer)
      end
    end
  end

  class TestEnglishQuiz < Test::Unit::TestCase
    def setup
      @eq = EnglishQuiz.new
    end

    def test_initialize
      assert_true(@eq.english.is_a?(Array))
      keys = %w!category en ja!

      @eq.english.each do |ew|
        keys.each do |k|
          assert_true(ew.has_key?(k))
        end
      end
    end

    def test_english_quiz
      quizzes = @eq.english_quiz
      assert_true(quizzes.is_a?(Array))
      assert_equal(3, quizzes.length)
      quizzes.each do |q|
        assert_not_nil(q.question)
        assert_not_nil(q.answer)
      end
    end
  end

  class TestWesternartQuiz < Test::Unit::TestCase
    def setup
      @waq = WesternArtQuiz.new
    end

    def test_initialize
      assert_true(@waq.westernart.is_a?(Array))
      keys = %w!name creator image city museum school!

      @waq.westernart.each do |wa|
        keys.each do |k|
          assert_true(wa.has_key?(k))
          assert_not_nil wa['image'].match(/^http/)
          assert_not_nil wa['name']
          assert_not_nil wa['creator']
        end
      end
    end

    def test_westernart_quiz
      len = @waq.westernart.length
      quizzes = @waq.westernart_quiz
      assert_equal len, @waq.westernart.length

      assert_true(quizzes.is_a?(Array))
      assert_equal(3, quizzes.length)
      quizzes.each do |q|
        assert_not_nil q.question
        assert_not_nil q.answer
        assert_not_nil q.description
      end

      assert_equal quizzes[0].answer, "ベラスケス"
      assert_equal quizzes[0].description, "ラス・メニーナス"

      quizzes = @waq.westernart_quiz
      assert_equal quizzes[1].answer, "ダヴィデ像"
      assert_equal quizzes[1].description, "ミケランジェロ"
    end

  end
end
