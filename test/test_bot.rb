# coding: utf-8

require 'slack-ruby-client'
require 'test/unit'
require './bot_main.rb'
require 'mocha'
require 'mocha/test_unit'

class TestBot < Test::Unit::TestCase
  def test_bot_main
    instance_eval File.read('./bot_main.rb')
    @handlers.each do |h| 
      assert_true(h[0].is_a?(Regexp))
      assert_true(h[1].is_a?(Proc))
    end

    @client = mock()
    @client.expects(:message).with(channel: 'test', text: 'Hi <@foo>!').once
    data = Hash.new
    data['text'] = 'bot hi'
    data['channel'] = 'test'
    data['user'] = 'foo'
    dispatch data
  end

  def test_bot_main2
    instance_eval File.read('./bot_main.rb')

    @client = mock()
    @client.expects(:message).with(channel: 'test', text: 'Hi <@foo>!').once
    data = Hash.new
    data['attachments'] = Array.new
    data['attachments'][0] = Hash.new
    data['attachments'][0]['pretext'] = 'bot hi'
    data['attachments']
    data['channel'] = 'test'
    data['user'] = 'foo'
    dispatch data
  end

  def test_add_handler
    @handlers = []
    add_handler /hoge/ do
      true
    end

    assert_equal(1, @handlers.size)
    h = @handlers[0]
    assert_true(h[0].is_a?(Regexp))
    assert_true(h[1].is_a?(Proc))
  end

end
