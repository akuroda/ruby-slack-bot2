# coding: utf-8

require 'slack-ruby-client'
require 'json'
require './bot_main'

class Slack::RealTime::Client
  attr_accessor :prev
end

Slack.configure do |config|
  config.token = ENV['SLACK_API_TOKEN']
  fail 'Missing ENV[SLACK_API_TOKEN]!' unless config.token
  config.logger = Logger.new(STDOUT)
  config.logger.level = Logger::INFO
#  config.logger.level = Logger::DEBUG
end

@fq = FlagQuiz.new
@mq = MathQuiz.new
@kq = KotowazaQuiz.new
@eq = EnglishQuiz.new
@waq = WesternArtQuiz.new

@webClient = Slack::Web::Client.new

@client = Slack::RealTime::Client.new
@client.prev = -1

@client.on :hello do
  puts 'Successfully connected.'
  @webClient.chat_postMessage channel: '#test', text: 'hello slack!'
end

@client.on :message do |data|
  puts "prev: " + @client.prev.to_s
  puts data
  dispatch data
end

@client.on :close do |_data|
  puts "Client is about to disconnect"
end

@client.on :closed do |_data|
  puts "Client has disconnected successfully!"
end

@client.start!
